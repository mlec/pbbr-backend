// server.js
var express    = require('express'),
    app        = express(),
    bodyParser = require('body-parser'),
    networkContoller = require('../controller/NetworkController'),
    pdfController = require('../controller/PDFController'),
    mailContoller = require('../controller/MailController'),
    path = require('path'),
    mime = require('mime');

// configure app to use bodyParser()
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// configure server settings
var port = process.env.PORT || 8080;

// Routes
var router = express.Router();

router.route('/estimate')
    .post(function(reqeust, response) {
        console.log('PBBR Server received estimate' + reqeust.body.key);
        fetchEstimateAndGenerateDocument(reqeust.body.key);
        response.json({ message: 'Status OK!' });
    });

// all of our routes will be prefixed with /api
app.use('/api', router);

// start the server

app.listen(port);
console.log('PBBR Express server ready on http://localhost:' + port);

// internal methods

var fetchEstimateAndGenerateDocument = function(key){
    console.log('Fetch full estimate information from firebase...');
    networkContoller.fetchEstimate(key, function(estimate){
        console.log('Start generting pdf document...');
        pdfController.generateDocument(key, estimate, function () {
            console.log('Sending mail...');
            mailContoller.sendMail(estimate, function () {
                // Write pdf to google drive
            });
        });
    });
};
