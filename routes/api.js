var Rest = require('connect-rest');

var options = {
    context : '/api',
    domain : require('domain').create()
};

options.domain.on('error', function (error){

    console.log(error);
});

module.exports = function(app) {
  
    var rest = Rest.create(options);
    app.use(rest.processRequest());
    
    rest.post('/test', function(request, content, callback) {

        console.log("Post eingegangen");
        callback(null, 'ok');
    });
};
