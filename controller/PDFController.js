/**
 * Created by christophmueller on 14.07.16.
 */
var fs = require('fs');

var PDFContoller = {
    
    generateDocument: function(key, estimate, onComplete) {

        var fonts = {
            Roboto: {
                normal: '../resources/fonts/arial.ttf',
                bold: '../resources/fonts/arial-bold.ttf'
            }
        };

        var PdfPrinter = require('../src/printer');
        var printer = new PdfPrinter(fonts);

        // Fetch services
        var services = [];
        // Add table header
        services.push([{ text: 'Nr.', style: 'tableHeader' }, { text: 'Bezeichnung', style: 'tableHeader'}, { text: 'Menge', style: 'tableHeader' }, { text: 'Einzelpreis', style: 'tableHeader' }, { text: 'Gesamtpreis', style: 'tableHeader' }]);
        //Add selected services
        estimate.estimate.services.forEach(function(service, index) {

            var positionNumber = index + 1;
            var estimateService = [];

            var rimModelName = '';
            if (positionNumber == 1 && estimate.rimDetails.modelName.length > 0){
                rimModelName = ' Felgenmodellbez.: ' + estimate.rimDetails.modelName;
            };

            estimateService.push({ text: positionNumber.toString(), style: 'normal' });
            estimateService.push({ text: service.name + rimModelName, style: 'normal' });
            estimateService.push({ text: service.quantity.toString(), style: 'normal' });
            estimateService.push({ text: service.price.toFixed(2).toString() + ' €', style: 'normal' });
            estimateService.push({ text: service.amount.toFixed(2).toString() + ' €', style: 'normal' });
            services.push(estimateService);
        });

        var docDefinition = {
            content: [

                {
                    // under NodeJS (or in case you use virtual file system provided by pdfmake)
                    // you can also pass file names here
                    image: '../resources/images/pbbr_schriftzug_schwarz_2_2.png',
                    width: 200,
                    height: 73,
                    alignment: 'right'
                },
                ' ',
                ' ',
                ' ',
                { text: '-P.B.B.R.- | Schnödeweg 2 | 35066 Frankenberg', style: 'subtitle', decoration: 'underline'},
                { text: 'Herrn', style: 'normal', margin: [0, 10, 0, 0]},
                { text: estimate.customer.firstName + ' ' + estimate.customer.lastName, style: 'normal'},
                { text: estimate.customer.street + ' ' + estimate.customer.house, style: 'normal'},
                { text: estimate.customer.postalCode + ' ' + estimate.customer.city, style: 'normal'},
                ' ',
                { text: estimate.customer.mail, style: 'normal'},
                { text: estimate.customer.mobilnumber, style: 'normal'},
                ' ',
                { text: 'Datum: 11.07.2016', style: 'normal', alignment: 'right'},
                { text: 'Ust.-ID-Nr.: DE293818694', style: 'normal', alignment: 'right', margin: [0, 10, 0, 0]},
                { text: 'Nr.: ' + key, style: 'normal', alignment: 'right'},
                ' ',
                ' ',
                { text: 'Kostenvoranschlag', style: 'headline'},
                { text: 'PBBR hält sich das Recht vor bei persönlicher Anlieferung preisliche Anpassungen vorzunehmen.', style: 'normal', margin: [0, 10, 0, 0]},
                ' ',
                ' ',
                //Table estiamte details
                {
                    table: {
                        headerRows: 1,
                        widths: [ '5%', '55%', '10%', '15%', '15%' ],

                        body: services
                    },
                    layout : 'headerLineOnly'
                },
                // Table estiamte total
                ' ',
                {
                    table: {
                        widths: [ '60%', '27%', '10%' ],

                        body: [
                            [   { text: ' ', style: 'normal' },
                                { text: 'Gesamtnetto', style: 'normal' },
                                { text: estimate.estimate.VAT.toFixed(2).toString() + ' €', style: 'normal' }
                            ],
                            [
                                { text: ' ', style: 'normal' },
                                { text: 'zzgl. MwSt. 19%', style: 'normal' },
                                { text: estimate.estimate.subTotal.toFixed(2).toString() + ' €', style: 'normal' }
                            ],
                            [
                                { text: ' ', style: 'normal' },
                                { text: 'Gesamtbetrag', style: 'normal', bold : true},
                                { text: estimate.estimate.totalPrice.toFixed(2).toString() + ' €', style: 'normal', bold : true }
                            ],
                        ]
                    },
                    layout : 'noBorders',
                },
                ' ',
                { text: estimate.estimate.comments, style: 'normal'}
            ],
            // Footer
            footer: {
                table: {
                    widths: [ '33%', '33%', '33%' ],
                    body: [
                        [
                            { text: 'P.B.B.R', style: 'subtitle', bold : true},
                            { text: 'Kontakt:', style: 'subtitle', bold : true, alignment: 'center'},
                            { text: 'Bankverbindung:', style: 'subtitle', bold : true, alignment: 'right'}
                        ],
                        [
                            { text: 'PulverBeschichtungsBetriebRusinow', style: 'subtitle' },
                            { text: 'E: info@pulverbeschichtung-rusinow.de', style: 'subtitle', alignment: 'center'},
                            { text: 'Volksbank Mittelhessen eG', style: 'subtitle', alignment: 'right'}
                        ],
                        [
                            { text: 'Geschäftsführer: Alexander Rusinow', style: 'subtitle' },
                            { text: 'T: +49 152 595 406 43', style: 'subtitle', alignment: 'center'},
                            { text: 'IBAN: DE22513900000027097405', style: 'subtitle', alignment: 'right'}
                        ],
                        [
                            { text: 'Am Ziegelhaus 32, 35066 Frankenberg', style: 'subtitle'},
                            { text: 'I: www.pulverbeschichtung-rusinow.de', style: 'subtitle', alignment: 'center'},
                            { text: 'BIC: VBMHDE5F', style: 'subtitle', alignment: 'right'}
                        ]
                    ]
                },
                layout : 'noBorders',
                margin: [50,70]
            },

            styles: {
                normal: {
                    fontSize: 10,
                },
                title: {
                    fontSize: 12,
                    bold: true
                },
                subtitle: {
                    fontSize: 8
                },
                headline: {
                    fontSize: 12,
                    bold: true
                },
                tableHeader: {
                    fontSize: 12,
                    bold: true,
                }
            },
            pageSize: 'LETTER',
            pageMargins: [50, 50, 50, 140]
        };

        var pdfStream = fs.createWriteStream('../output/Kostenvoranschlag.pdf');
        pdfStream.on('close', function() {
            console.log('Generate pdf document successfully');
            onComplete();
        });

        var pdfDoc = printer.createPdfKitDocument(docDefinition);
        pdfDoc.pipe(pdfStream);
        pdfDoc.end();
    }
};

module.exports = PDFContoller;