/**
 * Created by christophmueller on 21.07.16.
 */

var path = require('path');

var MailContoller = {

    sendMail: function(estimate, onComplete){

        var api_key = 'key-5cf413413f99fc1385798aefa898b445';
        var domain = 'pbbr-pulverbeschichtung.de';
        var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

        var pdf = path.join(__dirname, '../output/Kostenvoranschlag.pdf');

        var message = '<img src="http://pulverbeschichtung-rusinow.de/kostenvoranschlagtest/resources/images/pbbr/pbbr_schriftzug_schwarz_2_2_small.png"/>\
        <br>\
        <p>\
        <b>Hallo ' + estimate.customer.firstName + ',</b>\
        </p>\
        <p>\
        wir freuen uns über Dein Interesse an einer Pulverbeschichtung.\
        </p>\
        <p>\
        Folgend erhälst du deinen persönlichen Kostenvoranschlag.\
        </p>\
        <p>\
        PBBR hält sich das Recht vor bei persönlicher Anlieferung preisliche Anpassungen vorzunehmen.\
        <br>\
        Du hast Etwas falsches eingegeben oder einfach deine Meinung geändert? Kein Problem. Gerne kannst Du über den Felgenpreisgenerator einen neuen Kostenvoranschlag anstoßen.\
        </p>\
        <p>\
        <b>Wie geht es jetzt weiter?</b><br>\
        Sollte dir das vorliegende Angebot zusagen, dann anworte uns doch einfach auf diese E-Mail! Wir werden uns anschließend mit dir persönlich in Verbindung setzen, um die Einzelheiten abzustimmen.\
        </p>\
        <p>\
        Bei allen weiteren Fragen stehen wir Dir gerne zur Verfügung.\
        </p>\
        <p>\
        <b>Dein Team von PBBR</b>\
        </p>\
        <p>\
        Verwaltung:<br>\
        Frau S.Holzmann<br>\
        Am Ziegelhaus 32<br>\
        35066 Frankenberg/ Eder\
        </p>\
        <p>\
        Produktion:<br>\
        Herr A.Rusinow<br>\
        Schnödeweg 2<br>\
        35066 Frankenberg/ Eder\
        </p>\
        <p>\
        www.pulverbeschichtung-rusinow.de<br>\
        www.pbbr.one<br>\
        E: info@pulverbeschichtung-rusinow.de<br>\
        T: +49 152 595 406 43\
        </p>'
        
        var data = {
            from: 'info@pbbr.one',
            to: 'mueller.christoph92@icloud.com',
            subject: 'Dein Kostenvoranschlag bei PBBR Pulverbeschichtung',
            html: message,
            attachment : pdf
        };

        mailgun.messages().send(data, function (error, body) {
            console.log('Mail send successfully');
            console.log('Response from MailGun ' + body);
            onComplete();
        });
    }

};

module.exports = MailContoller;


