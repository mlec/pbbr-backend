/**
 * Created by christophmueller on 14.07.16.
 */

var firebase = require('firebase');

var NetworkContoller = {
    
    fetchEstimate: function(key, onComplete){

    var app = firebase.initializeApp({
        apiKey: "AIzaSyCDGVmRECKbg46ZBHtuJ_aWxLZEdcqfbDY",
        authDomain: "project-6966931246366534264.firebaseapp.com",
        databaseURL: "https://project-6966931246366534264.firebaseio.com",
        storageBucket: ""
    });

    var firebaseRef = firebase.database().ref();

    firebaseRef.child(key).on("value", function(snapshot) {
        console.log('Fetch estimate from firebase successfully');
        var estimate = snapshot.val();
        onComplete(estimate);
    });
    
    }

};

module.exports = NetworkContoller;